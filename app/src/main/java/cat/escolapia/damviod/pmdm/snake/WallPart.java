package cat.escolapia.damviod.pmdm.snake;

/**
 * Created by marc.guillen on 12/12/2016.
 */
public class WallPart {
    public  int x;
    public  int y;
    public WallPart(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
}
