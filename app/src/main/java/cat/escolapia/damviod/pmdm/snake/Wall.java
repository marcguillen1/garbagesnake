package cat.escolapia.damviod.pmdm.snake;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marc.guillen on 07/12/2016.
 */
public class Wall {
    public static List<WallPart> wallparts = new ArrayList<WallPart>();

    public Wall() {
        wallparts.add(new WallPart(2, 2));
        wallparts.add(new WallPart(2, 3));
        wallparts.add(new WallPart(2, 4));
        wallparts.add(new WallPart(2, 5));
    }
}

