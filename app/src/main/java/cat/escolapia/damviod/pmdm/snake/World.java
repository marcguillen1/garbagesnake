package cat.escolapia.damviod.pmdm.snake;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class World {
    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 13;
    static final int SCORE_INCREMENT = 10;
    static final float TICK_INITIAL = 0.5f;
    static final float TICK_DECREMENT = 0.04f;

    public Snake snake;
    public Diamond diamond;
    public Wall wall;
    public List<Diamond> diamondlist = new ArrayList<Diamond>();
    public boolean gameOver = false;;
    public int score = 0;

    boolean fields[][] = new boolean[WORLD_WIDTH][WORLD_HEIGHT];
    Random random = new Random();
    float tickTime = 0;
    float tick = TICK_INITIAL;

    public World() {
        snake = new Snake();
        wall = new Wall();
        placeDiamond(1);
    }

    private void placeDiamond(int nDia) {
        for(int j = 0; j < nDia; j++) {
            for (int x = 0; x < WORLD_WIDTH; x++) {
                for (int y = 0; y < WORLD_HEIGHT; y++) {
                    fields[x][y] = false;
                }
            }

            int len = snake.parts.size();
            for (int i = 0; i < len; i++) {
                SnakePart part = snake.parts.get(i);
                fields[part.x][part.y] = true;
            }

            int diamondX = random.nextInt(WORLD_WIDTH);
            int diamondY = random.nextInt(WORLD_HEIGHT);
            while (true) {
                if (fields[diamondX][diamondY] == false) break;
                diamondX += 1;
                if (diamondX >= WORLD_WIDTH) {
                    diamondX = 0;
                    diamondY += 1;
                    if (diamondY >= WORLD_HEIGHT) {
                        diamondY = 0;
                    }
                }
            }
            diamondlist.add(new Diamond(diamondX, diamondY, Diamond.TYPE_1));
        }
    }

    public void update(float deltaTime) {
        if (gameOver) return;

        tickTime += deltaTime;

        while (tickTime > tick) {
            tickTime -= tick;
            snake.advance();
            if (snake.checkXoca() || snake.checkWall()) {
                gameOver = true;
                return;
            }
            for(int i =0; i< diamondlist.size(); i++) {
                diamond = diamondlist.get(i);
                if (snake.checkDiamant(diamond)) {
                    snake.allarga();
                    diamondlist.remove(diamond);
                    score += SCORE_INCREMENT;
                    if(score < 40)placeDiamond(1);
                    else if(score >= 40 && diamondlist.isEmpty())placeDiamond(2);
                    else if(score >= 80 && diamondlist.isEmpty())placeDiamond(3);
                    tick -= TICK_DECREMENT;
                }
            }
            //Comprovació x i y dels head igual a diamant.
            // Si sí, score = score + INCREMENTSCORE, allarga serp i cridar a placeDiamond de nou... adicionalment
            // es pot baixar el tick per augmentar la dificultat
        }
    }
}
