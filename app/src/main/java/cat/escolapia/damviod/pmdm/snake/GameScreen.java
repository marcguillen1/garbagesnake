package cat.escolapia.damviod.pmdm.snake;

import java.util.List;

import android.graphics.Color;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Input.TouchEvent;
import cat.escolapia.damviod.pmdm.framework.Music;
import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Screen;

public class GameScreen extends Screen {
    enum GameState {
        Ready,
        Running,
        Paused,
        GameOver
    }
    
    GameState state = GameState.Ready;
    World world;
    String score = "0";
    int _lastX;
    int _lastY;
    
    public GameScreen(Game game) {
        super(game);
        world = new World();
    }

    @Override
    public void update(float deltaTime) {
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();
        
        if(state == GameState.Ready) {
            updateReady(touchEvents);
        }
        if(state == GameState.Running) {
            updateRunning(touchEvents, deltaTime);
            Assets.mainMenuMusic.stop();
            Assets.gameMusic.play();
        }
        if(state == GameState.Paused) {
            updatePaused(touchEvents);
            Assets.gameMusic.pause();
        }
        if(state == GameState.GameOver) {
            updateGameOver(touchEvents);
            Assets.gameMusic.pause();
        }
    }

    private void updateReady(List<TouchEvent> touchEvents) {
        if(touchEvents.size() > 0) state = GameState.Running;
    }

    private void updateRunning(List<TouchEvent> touchEvents, float deltaTime) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                int distX = event.x - _lastX;
                int distY = event.y -_lastY;
                if(Math.abs(distX) + Math.abs(distY) > 10) {
                    boolean verticalmoviment = false;
                    if (Math.abs(distX) < Math.abs(distY)) verticalmoviment = true;
                    if(verticalmoviment)
                    {
                        if(distY>0)world.snake.turnDown();
                        else world.snake.turnUp();
                    }else{
                        if(distX>0)world.snake.turnRight();
                        else world.snake.turnLeft();
                    }
                    Assets.click.play(1);
                }
            if(event.x > 0 && event.x < 45)
            {
                if(event.y > 0 && event.y < 45)
                {
                    state = GameState.Paused;
                }
            }
            }
            if(event.type == TouchEvent.TOUCH_DOWN) {
                _lastX = event.x; _lastY = event.y;
            }
        }

        world.update(deltaTime);
        score = Integer.toString(world.score);

        if(world.gameOver) {
            Assets.xoc.play(1);
            state = GameState.GameOver;
        }

    }

    private void updatePaused(List<TouchEvent> touchEvents) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x > 80 && event.x <= 240) {
                    if(event.y > 100 && event.y <= 148) {
                        Assets.click.play(1);
                        state = GameState.Running;
                        return;
                    }
                    if(event.y > 148 && event.y < 196) {
                        Assets.click.play(1);
                        game.setScreen(new MainMenuScreen(game));
                        return;
                    }
                }
            }
        }
    }

    private void updateGameOver(List<TouchEvent> touchEvents) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x >= 128 && event.x <= 192 &&
                   event.y >= 200 && event.y <= 264) {
                    Assets.click.play(1);
                    game.setScreen(new MainMenuScreen(game));
                    return;
                }
            }
        }
    }

    @Override
    public void render(float deltaTime) {
        Graphics g = game.getGraphics();
        if(world.score <= 20)g.drawPixmap(Assets.background1, 0, 0);
        else if(world.score >= 30 && world.score <=50)g.drawPixmap(Assets.background2, 0, 0);
        else if(world.score >= 60 && world.score <= 80)g.drawPixmap(Assets.background3, 0, 0);
        else if(world.score >= 90)g.drawPixmap(Assets.background4, 0, 0);
        drawWorld(world);

        if(state == GameState.Ready)
            drawReadyUI();
        if(state == GameState.Running)
            drawRunningUI();
        if(state == GameState.Paused)
            drawPausedUI();
        if(state == GameState.GameOver)
            drawGameOverUI();
        
        drawText(g, score, g.getWidth() / 2 - score.length()*20 / 2, g.getHeight() - 42);
    }

    private void drawWorld(World world) {
        Graphics g = game.getGraphics();
        Snake snake = world.snake;
        SnakePart head = snake.parts.get(0);
        Wall wall = world.wall;
        List<Diamond> diamond = world.diamondlist;

        int x = 0;
        int y = 0;

        for(int i = 0; i < diamond.size(); i++) {
            Pixmap diamonPixmap = null;
            if (diamond.get(i).type == Diamond.TYPE_1) diamonPixmap = Assets.diamond;
            x = diamond.get(i).x * 32;
            y = diamond.get(i).y * 32;
            g.drawPixmap(diamonPixmap, x, y);
        }

        int len = snake.parts.size();
        for(int i = 1; i < len; i++) {
            SnakePart part = snake.parts.get(i);
            x = part.x * 32;
            y = part.y * 32;
            g.drawPixmap(Assets.tail, x, y);
        }

        int length = Wall.wallparts.size();
        for(int i = 0; i < length; i++) {
            WallPart part = Wall.wallparts.get(i);
            x = part.x * 32;
            y = part.y * 32;
            g.drawPixmap(Assets.wall, x, y);
        }
        
        Pixmap headPixmap = null;
        if(snake.direction == Snake.UP) {
            headPixmap = Assets.headUp;
        }
        if(snake.direction == Snake.LEFT) {
            headPixmap = Assets.headLeft;
        }
        if(snake.direction == Snake.DOWN) {
            headPixmap = Assets.headDown;
        }
        if(snake.direction == Snake.RIGHT) {
            headPixmap = Assets.headRight;
        }
        x = head.x * 32 + 17;
        y = head.y * 32 + 17;
        g.drawPixmap(headPixmap, x - headPixmap.getWidth() / 2, y - headPixmap.getHeight() / 2);
    }

    private void drawReadyUI() {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.ready, 0, 0);
        g.drawLine(0, 416, 480, 416, Color.WHITE);
    }
    
    private void drawRunningUI() {
        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.buttons, 0, 0, 64, 128, 64, 64);
        g.drawLine(0, 416, 480, 416, Color.WHITE);
    }
    
    private void drawPausedUI() {
        Graphics g = game.getGraphics();
        
        g.drawPixmap(Assets.pause, 80, 100);
        g.drawLine(0, 416, 480, 416, Color.RED);
    }

    private void drawGameOverUI() {
        Graphics g = game.getGraphics();
        
        g.drawPixmap(Assets.gameOver, 62, 100);
        g.drawPixmap(Assets.buttons, 128, 200, 0, 128, 64, 64);
        g.drawLine(0, 416, 480, 416, Color.BLACK);
    }
    
    public void drawText(Graphics g, String line, int x, int y) {
        int len = line.length();
        for (int i = 0; i < len; i++) {
            char character = line.charAt(i);

            if (character == ' ') {
                x += 20;
                continue;
            }

            int srcX = 0;
            int srcWidth = 0;
            if (character == '.') {
                srcX = 200;
                srcWidth = 10;
            } else {
                srcX = (character - '0') * 20;
                srcWidth = 20;
            }

            g.drawPixmap(Assets.numbers, x, y, srcX, 0, srcWidth, 32);
            x += srcWidth;
        }
    }

    @Override
    public void pause() {
        if(state == GameState.Running)
            state = GameState.Paused;
        
        if(world.gameOver) {
            Settings.addScore(world.score);
            Settings.save(game.getFileIO());
        }
    }

    @Override
    public void resume() {
        
    }

    @Override
    public void dispose() {
        
    }
}

